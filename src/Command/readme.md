**脚本介绍：**

|脚本|功能|备注|
|---|---|---|
|cmds|显示所有自定义命令||
|clearMavenInvalidFile|清除 Maven 本地仓库的全部无效文件||
|installJarToLocalMavenRepo|安装 Jar 包到本地 Maven 仓库||
|ll|模拟个别 Linux 平台上的 `ll` 命令||
|sshpass|模拟 sshpass 非交互式 ssh 登录||
|sshs|ssh 连接管理工具|依赖于 `sshpass` 命令|