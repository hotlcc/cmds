# cmds

> MacOS/Linux下的自建命令方案

作者：Allen

码云主页：[https://gitee.com/hotlcc](https://gitee.com/hotlcc)

[![gitee](https://img.shields.io/badge/github-%40hotlcc-blank.svg)](https://github.com/hotlcc)
[![License](https://img.shields.io/badge/license-Anti%20996-4EB1BA.svg)](https://raw.githubusercontent.com/996icu/996.ICU/master/LICENSE_CN)
<a href="https://996.icu"><img src="https://img.shields.io/badge/link-996.icu-red.svg"></a>

## 自建命令方案

> 适用于 unix 系 OS 环境

- 创建自建命令目录

```shell
mkdir ~/Command
```

- 配置环境变量

```shell
vi ~/.bash_profile
```

添加内容：

```shell
# 自建命令目录
export USER_COMMAND_PATH=~/Command
export PATH=$PATH:$USER_COMMAND_PATH
```

- 然后将 `src/` 中命令放到 `~/Command` 中

- `src/` 中 `cmds` 命令的作用就是列出所有自建命令（不含 `cmds` 本身）

## 一键配置

**在终端执行如下代码可一键配置：**

```shell
bash <(curl "https://gitee.com/hotlcc/cmds/raw/master/doc/cmds_auto_config.sh")
```

## 欢迎提交更多实用的命令

请遵循如下规范：

- 脚本中不含任何违法信息、个人隐私或其它不宜公示的内容；
- 脚本名无 `.sh` 后缀；
- 内容需包含完整的注释信息；

### 模板建议

#### shell

```shell
#!/bin/bash
# ==============================
# 脚本名称：xxxx.sh
# 中文名称：xx脚本
# 功能描述：<功能描述>
# 开发作者：Allen
# 依赖命令：<依赖命令列表>
# ==============================

# ========== 预定义变量 ========= # 定义一些全局变量或配置

# ========== 脚本预处理 ========= # 处理一些前期行为

# ========== 预定义函数 ========= # 定义一些功能函数

# ========== 主程序区 =========== # 程序运行起始位置
```

#### python

```python
#!/usr/bin/python
# -*- coding: UTF-8 -*-
# ==============================
# 脚本名称：xxxx.py
# 中文名称：xx脚本
# 功能描述：<功能描述>
# 开发作者：Allen
# 依赖命令：<依赖命令列表>
# ==============================

# ========== 依赖导入 =========== # import 外部依赖

import sys, logging

# ========== 预定义常量 ========= #

# ========== 预定义函数 ========= #

# 获取python主版本
def get_python_main_version():
    return int(sys.version.split(".")[0])

# ========== 预定义类 =========== #

# ========== 前置处理 =========== #

# 前置处理

# 配置日志工具
logging.basicConfig(
    level = logging.INFO,
    format = '%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s'
)

main_version = get_python_main_version()
if (main_version == 2):
    reload(sys)
    sys.setdefaultencoding('utf8')
elif (main_version == 3):
    import importlib
    importlib.reload(sys)

# 脚本参数处理

# ========== 主程序区 =========== #

```
