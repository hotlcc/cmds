#!/bin/bash
# ==============================
# 脚本名称： cmds_auto_config.sh
# 脚本功能：自建命令方案自动配置脚本
# 开发作者：Allen
# 依赖命令：curl | unzip
# ==============================

# ========== 预定义变量 ========== # 定义一些全局变量或配置
# 环境变量文件
bash_profile=~/.bash_profile
# zip 下载 url
zip_url="https://gitee.com/hotlcc/cmds/repository/archive/master.zip"
# 默认的自建命令目录
cmds_dir=~/Command
cmds_dir_str="~/Command"

# ========== 脚本预处理 ========== # 处理一些前期行为
# 临时目录前缀
temp_dir_prefix=$(basename $0)

# ========== 预定义函数 ========== # 定义一些功能函数
# 打印日志
function log () {
    # type: debug info warn error
    local readonly type=$1; local readonly line=$2; local readonly msg=$3
    
    # type 大写处理
    local readonly TYPE=$(echo $type | awk '{print toupper($0)}')
    echo -e "[$TYPE] $msg\t<$line>" >&2
    return 0
}
# 创建临时目录
function mktemp_dir () {
    local prefix=$1

    local dir=$(mktemp -d /tmp/$prefix.XXXXXXXXXX)
    echo $dir
    return 0
}

# ========== 主程序区 ========== # 程序运行起始位置
# 检查默认的自建目录
if [ -d $cmds_dir ]; then
    log warn $LINENO "默认的自建命令目录\"$cmds_dir_str\"已经存在，为了防止文件污染，原则上不允许继续操作，除非您确认不会出现任何问题."
    read -r -p "       是否允许继续操作？ [Y/n] " input
    case $input in
    [yY][eE][sS]|[yY])
        ;;
    [nN][oO]|[nN])
        log info $LINENO "您已取消继续操作."; exit 2
       	;;
    *)
        log error $LINENO "无效输入，操作中断."; exit 3
        ;;
    esac
fi
# 创建临时工作目录
temp_dir=$(mktemp_dir $temp_dir_prefix)
# 下载 zip 文件
log info $LINENO "开始下载zip文件..."
temp_zip_file="$temp_dir/$(basename $zip_url)"
curl -o "$temp_zip_file" $zip_url
if [ $? -eq 0 ]; then log info $LINENO "zip文件下载完成: $temp_zip_file"; else log error $LINENO "zip文件下载失败."; fi
# 创建自建命令目录
mkdir -p $cmds_dir
# 解压 zip 文件
log info $LINENO "开始解压zip文件..."
unzip -oq $temp_zip_file -d $temp_dir
log info $LINENO "zip文件解压完成."
# 拷贝命令文件
log info $LINENO "开始拷贝命令文件..."
num=0; for i in $(ls $temp_dir/cmds/src/Command/ | grep -v "\.md"); do cp -f $temp_dir/cmds/src/Command/$i $cmds_dir/; num=$(expr $num + 1); done
log info $LINENO "命令文件拷贝完成，共"$num"个."
# 清除临时目录
rm -rf $temp_dir
# 配置环境变量
log info $LINENO "开始配置环境变量..."
echo -e "\n# 自建命令环境变量\nexport USER_COMMAND_PATH=$cmds_dir_str\nexport PATH=\$PATH:\$USER_COMMAND_PATH\n" >> $bash_profile
source $bash_profile
log info $LINENO "环境变量配置完成."
# 完成
log info $LINENO "自建命令配置完成."
